//
// Created by thephosphorus on 9/29/19.
//

#ifndef USER_SERVICE_RESTSERVER_H
#define USER_SERVICE_RESTSERVER_H

#include <nlohmann/json.hpp>
#include <restbed>

#include <queue>
#include <cstddef>
#include <iostream>
#include <memory>

namespace yx {

namespace Rest {
class Request;

class Response;

using Session = std::shared_ptr<restbed::Session>;
using NativeRequest = std::shared_ptr<const restbed::Request>;
using NativeResponse = std::shared_ptr<restbed::Response>;

using RequestHandler =
    std::function<void(const Rest::Request &, Rest::Response &)>;
using NativeRequestHandler = std::function<void(const Session)>;

using Json = nlohmann::json;
} // namespace Rest

class RestServer {
public:
  // Constructor
  RestServer(const std::string &host, size_t port);

  // Constructor from ConfigHelper
  RestServer();

  // Add Route
  void get(const std::string &route,
           const Rest::RequestHandler &requestHandler);

  void post(const std::string &route,
            const Rest::RequestHandler &requestHandler);

  void put(const std::string &route,
           const Rest::RequestHandler &requestHandler);

  void del(const std::string &route,
           const Rest::RequestHandler &requestHandler);

  void handleRequest(const std::string &type, const std::string &route,
                     const Rest::RequestHandler &requestHandler);

  void start();

  [[nodiscard]] size_t getPort() const;
  [[nodiscard]] const std::string &getHost() const;

private:
  void init(const std::string &host, size_t port);
  void cleanReq();

private:
  std::shared_ptr<restbed::Settings> _settings;
  restbed::Service _service;

  size_t _port;

private:
  std::string _host;
  std::queue<std::unique_ptr<Rest::Request>> _requests;

}; // class RestServer

namespace Rest {

class Response {
public:
  explicit Response(const Rest::Session &session);

  void returnStatus(int status);

  void send(int status, const std::string &message);

  void sendJson(int status, const Rest::Json &body);

private:
  const std::shared_ptr<restbed::Session> _session;
};

class Request {
public:
  explicit Request(const Rest::Session &session);

  NativeRequest native();

  [[nodiscard]] Rest::Json body() const;
  
  [[nodiscard]] std::string accessToken() const;

  [[nodiscard]] std::string getParam(const std::string& param) const;

  void onReady(const Rest::RequestHandler &callback);

  bool isDone();

private:
  const std::shared_ptr<restbed::Session> _session;
  bool _ready{false};
  Rest::RequestHandler _callback;
  Response _response;

  bool _isDone{false};

  std::string _body;
};
} // namespace Rest

} // namespace yx

#endif // USER_SERVICE_RESTSERVER_H
