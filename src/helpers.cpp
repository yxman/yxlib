#include "yxlib/helpers.h"
#include <iomanip>

LogHeader::LogHeader(unsigned logID, std::string &logSource, std::string &type)
    : _nLog(logID), _logSource(logSource), _type(type) {
  time_t currentTime = time(nullptr);
  _time = localtime(&currentTime);
}

std::ostream &operator<<(std::ostream &os, const LogHeader &lh) {
  os << mktime(lh._time) << " [ " << lh._type << ' '
     << std::setfill('0') << std::setw(2) << lh._time->tm_hour << ':'
     << std::setfill('0') << std::setw(2) << lh._time->tm_min << ':'
     << std::setfill('0') << std::setw(2) << lh._time->tm_sec << ' '
     << lh._logSource << " ] ";
  return os;
}
