
#ifndef MINER_APP_COMMHELPER_H
#define MINER_APP_COMMHELPER_H

#include <memory>

#include <zmq.hpp>

namespace yx {

    class CommHelper {
    public:
        static constexpr auto TYPE_FIELD= "type";
        static constexpr auto REQUEST_FIELD= "request";

        struct Request{
            int type;
            std::string params;
        };

        struct Option{
            int key;
            int value;
        };

        static std::shared_ptr<zmq::socket_t> initConnection(std::shared_ptr<zmq::context_t>& context,
                                                             const std::string& endpoint, zmq::socket_type type, bool isBind,
                                                             const std::vector<Option> &options = std::vector<Option>());
        static std::string receiveString(std::shared_ptr<zmq::socket_t>& socket,zmq::recv_flags flag =  zmq::recv_flags::none);
        static bool sendString(std::shared_ptr<zmq::socket_t>& socket, const std::string& message,
                zmq::send_flags flag =  zmq::send_flags ::none);
        static bool sendRequest(std::shared_ptr<zmq::socket_t>& socket,int type, const std::string& request,
                zmq::send_flags flag =  zmq::send_flags ::none);
        static Request receiveRequest(std::shared_ptr<zmq::socket_t>& socket, zmq::recv_flags flag = zmq::recv_flags::none);

    };

}

#endif //MINER_APP_COMMHELPER_H
