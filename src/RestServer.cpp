//
// Created by thephosphorus on 9/29/19.
//

#include "yxlib/RestServer.h"
#include "yxlib/ConfigHelper.h"

#include <string>

using namespace yx;

using Json = nlohmann::json;

RestServer::RestServer(const std::string &host, const size_t port)
    : _settings(std::make_shared<restbed::Settings>()) {
  init(host, port);
}

void RestServer::post(const std::string &route,
                      const Rest::RequestHandler &requestHandler) {
  handleRequest("POST", route, requestHandler);
}

void RestServer::get(const std::string &route,
                     const Rest::RequestHandler &requestHandler) {
  handleRequest("GET", route, requestHandler);
}

void RestServer::put(const std::string &route,
                     const Rest::RequestHandler &requestHandler) {
  handleRequest("PUT", route, requestHandler);
}

void RestServer::del(const std::string &route,
                     const Rest::RequestHandler &requestHandler) {
  handleRequest("DELETE", route, requestHandler);
}

void RestServer::handleRequest(const std::string &type,
                               const std::string &route,
                               const Rest::RequestHandler &requestHandler) {
  cleanReq();
  auto resource = std::make_shared<restbed::Resource>();
  resource->set_path(route);
  resource->set_method_handler(
      type, [this, requestHandler, route](const Rest::Session &session) {
        std::cout << "received on " << route << "." << std::endl;
        // Create the request
        _requests.emplace(std::make_unique<Rest::Request>(session));
        _requests.back()->onReady(requestHandler);
      });
  _service.publish(resource);
}

void RestServer::start() { _service.start(_settings); }

RestServer::RestServer() : _settings(std::make_shared<restbed::Settings>()) {
  ConfigSection section = ConfigHelper::getSection("rest_api");
  init(section.getStr("host"), section.getUint("port"));
}

void RestServer::init(const std::string &host, size_t port) {
  _port = port;
  _host = host;
  _settings->set_bind_address(host);
  _settings->set_port(port);
  _settings->set_default_header("Connection", "close");
}
const std::string &RestServer::getHost() const { return _host; }
size_t RestServer::getPort() const { return _port; }
void RestServer::cleanReq() {
  while (!_requests.empty() && _requests.front()->isDone()) {
    _requests.pop();
  }
}
Rest::Request::Request(const Rest::Session &session)
    : _session(session), _response(session) {
  const auto request = _session->get_request();
  const size_t content_length = request->get_header("Content-Length", 0);

  _session->fetch(content_length,
                  [this](const Session &session, const restbed::Bytes &body) {
                    _body.reserve(body.size());
                    for (restbed::Byte byte : body) {
                      _body.push_back(byte);
                    }

                    _ready = true;
                    if (_callback) {
                      _callback(*this, _response);
                      _isDone = true;
                    }
                  });
}

Json Rest::Request::body() const { return Json::parse(_body); }

void Rest::Request::onReady(const Rest::RequestHandler &callback) {
  if (_ready) {
    callback(*this, _response);
    _isDone = true;
  } else {
    _callback = callback;
  }
}

Rest::NativeRequest Rest::Request::native() { return _session->get_request(); }
std::string Rest::Request::accessToken() const {
  try {
    constexpr auto tokenField = "Authorization";
    std::multimap<std::string, std::string> headers =
        _session->get_request()->get_headers();
    auto it = headers.find(tokenField);

    return (it != _session->get_headers().end()) ? it->second : "";
  } catch (const std::exception &exception) {
    return "";
  }
}
bool Rest::Request::isDone() { return _isDone; }
std::string Rest::Request::getParam(const std::string &param) const {
  try {
    return _session->get_request()->get_path_parameter( param);
  } catch (const std::exception& exception)
  {
    return "";
  }
  return std::string();
}

Rest::Response::Response(const Rest::Session &session) : _session(session) {}

void Rest::Response::returnStatus(int status) {
  Json body;

  body["message"] = "Program closed the session";
  body["status"] = status;

  sendJson(status, body);
}

void Rest::Response::send(int status, const std::string &message) {
  Json body;

  body["message"] = message;
  body["status"] = status;

  sendJson(status, body);
}

void Rest::Response::sendJson(int status, const Rest::Json &body) {
  std::cout << "Returned " << status << std::endl;
  const std::string bodyStr = body.dump();

  _session->close(status, bodyStr,
                  {{"Content-Length", std::to_string(bodyStr.size())},
                   {"Content-Type", "application/json"}});
}
