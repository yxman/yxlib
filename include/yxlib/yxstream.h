#ifndef YXLIB_YXSTREAM_H
#define YXLIB_YXSTREAM_H

#include "helpers.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <memory>

namespace yx {
class yxstream {
public:
  enum TYPE { INFO, WARNING, ERROR };
  explicit yxstream(TYPE type);
  yxstream(std::string &type, std::ostream *os1, std::ostream *os2);
  yxstream(std::string &type, std::ostream *os1, std::ostream *os2,
           std::ostream *os3);
  yxstream(std::string &type, std::vector<std::ostream*>os);

  friend yxstream &operator<<(yxstream &yxos,
                              std::ostream &(*manipElement)(std::ostream &));

  template <class T> friend yxstream &operator<<(yxstream &yxos, T data);

  template <class M>
  friend yxstream &
  operator<<(yxstream &yxos, std::ostream &(*manipFunction)(std::ostream &, M));

private:
  void loadConfig();
  void updateHeader();

private:
  std::vector<std::ostream*>_osStreams;
  std::vector<std::unique_ptr<std::ofstream>> _openedFiles;
  std::string _type;
  std::string _location;
  std::string _logPath;
  bool _isNewLog{true};
  static unsigned _nLog;
};

template <class T> // Template must be defined in .h
// Redirect streamed data
yxstream &operator<<(yxstream &yxos, T data) {
  if (yxos._isNewLog) { // if it is a new log, add the header
    yxos._isNewLog =
        false; // after adding header, we are in the same log until an endl
    yxos.updateHeader();
  }
  for (std::ostream* stream: yxos._osStreams) {
    *stream << data;
  }
  return yxos;
}

template <class M> // Template must be defined in .h
// Redirect manipulator functions
yxstream &operator<<(yxstream &yxos,
                     std::ostream &(*manipFunction)(std::ostream &, M)) {
  if (yxos._isNewLog) { // if it is a new log, add the header
    yxos._isNewLog =
        false; // after adding header, we are in the same log until an endl
    yxos.updateHeader();
  }
  for (std::ostream* stream: yxos._osStreams) {
    *stream << manipFunction;
  }
  return yxos;
}

yxstream& cout();
yxstream& cerr();
yxstream& cwar();

} // namespace yx
#endif // YXLIB_YXSTREAM_H