//
// Created by thephosphorus on 9/30/19.
//

#include "yxlib/DBManager.h"
#include "yxlib/ConfigHelper.h"

#include <iostream>
#include <memory>
#include <string>

using namespace yx;

DBManager::DBManager(size_t type, const std::string &host,
                     const std::string &port, const std::string &db,
                     const std::string &user, const std::string &password) {
  init(type, host, port, db, user, password);
}

std::string DBManager::generateConnectionString(size_t type,
                                                const std::string &host,
                                                const std::string &port,
                                                const std::string &db,
                                                const std::string &user,
                                                const std::string &password) {
  // Check the database type
  if (type > _dbTypes.size()) {
    std::cerr << "Database type not valid" << std::endl;
    return "";
  }

  std::string connectionStr = std::string(_dbTypes.begin()[type]) + "://";

  if (!db.empty())
    connectionStr += " dbname=" + db;
  if (!host.empty())
    connectionStr += " host=" + host;
  if (!port.empty())
    connectionStr += " port=" + port;
  if (!user.empty())
    connectionStr += " user=" + user;
  if (!password.empty())
    connectionStr += " password=" + password;

  return connectionStr;
}

DBManager::DBManager() {
  auto section = ConfigHelper::getSection("database");

  init(section.getUint("type"), section.getStr("host"), section.getStr("port"),
       section.getStr("db"), section.getStr("user"), section.getStr("pass"));
}

void DBManager::init(size_t type, const std::string &host,
                     const std::string &port, const std::string &db,
                     const std::string &user, const std::string &password) {
  std::string connectionStr;
  if (type > _dbTypes.size()) {
    std::cerr << "Database type not valid" << std::endl;
  } else {
    connectionStr =
        generateConnectionString(type, host, port, db, user, password);
  }

  _session = std::make_unique<soci::session>(connectionStr);
}

soci::session &DBManager::sql() { return *_session; }
