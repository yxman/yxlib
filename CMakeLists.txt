cmake_minimum_required(VERSION 3.8)

project(yxlib)

# Set CPP 17 flags
set( CMAKE_CXX_STANDARD 17 )
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Add file directories
set( SOURCE_DIR  "${PROJECT_SOURCE_DIR}/src" )
set( INCLUDE_DIR "${PROJECT_SOURCE_DIR}/include")

# Include Libraries
if ( NOT DEFINED LIB_DEPENDECIES_DIR )
    set(LIB_DEPENDECIES_DIR ${PROJECT_SOURCE_DIR}/lib)
endif()

## Resbed
option(NEED_RESTBED "restbed" TRUE)
option(NEED_YAML "yaml" TRUE)
option(NEED_SOCI "soci" TRUE)
option(NEED_JSON "json" TRUE)
option(NEED_CPR "cpr" TRUE)
option(NEED_ZMQ "zmq" TRUE)

# Add Library files
include(libIncludes.cmake)


# Adding files
## Adding ConfigHelper // Needs YAML
if ( NEED_YAML )
    list(APPEND SOURCES ${SOURCE_DIR}/ConfigHelper.cpp)
    list(APPEND ARTIFACTS ${INCLUDE_DIR}/yxlib/ConfigHelper.h)
endif()

## Adding DBManager // Needs ConfigHelper, SOCI
if ( NEED_YAML AND NEED_SOCI )
    list(APPEND SOURCES ${SOURCE_DIR}/DBManager.cpp)
    list(APPEND ARTIFACTS ${INCLUDE_DIR}/yxlib/DBManager.h)
endif()

## Adding RestServer // Needs ConfigHelper, Restbed, JSON
if ( NEED_YAML AND NEED_RESTBED AND NEED_JSON )
    list(APPEND SOURCES ${SOURCE_DIR}/RestServer.cpp)
    list(APPEND ARTIFACTS ${INCLUDE_DIR}/yxlib/RestServer.h)
endif()

## Adding OathHandler // Needs ConfigHelper, RestServer
if ( NEED_YAML AND NEED_RESTBED AND NEED_JSON AND NEED_CPR )
    list(APPEND SOURCES ${SOURCE_DIR}/oauthHandler.cpp)
    list(APPEND ARTIFACTS ${INCLUDE_DIR}/yxlib/oauthHandler.h)
endif()

## Adding YxStream // Needs ConfigHelper
if ( NEED_YAML )
    list(APPEND SOURCES ${SOURCE_DIR}/yxstream.cpp ${SOURCE_DIR}/helpers.cpp )
    list(APPEND ARTIFACTS ${INCLUDE_DIR}/yxlib/yxstream.h ${INCLUDE_DIR}/yxlib/helpers.h)
endif()

## Adding Models // Needs RestServer
if ( NEED_RESTBED )
    list(APPEND SOURCES ${SOURCE_DIR}/Models.cpp )
    list(APPEND ARTIFACTS ${INCLUDE_DIR}/yxlib/Models.h)
endif()

## Adding CommHelper
if ( NEED_ZMQ AND NEED_YAML)
    list(APPEND SOURCES ${SOURCE_DIR}/CommHelper.cpp )
    list(APPEND ARTIFACTS ${INCLUDE_DIR}/yxlib/CommHelper.h)
endif()

set( YX_LIB_NAME "${PROJECT_NAME}")
add_library(${YX_LIB_NAME} SHARED ${SOURCES})
set_target_properties(${YX_LIB_NAME} PROPERTIES OUTPUT_NAME ${PROJECT_NAME})
set_target_properties(${YX_LIB_NAME} PROPERTIES PREFIX "")
target_link_libraries(${YX_LIB_NAME} Boost::filesystem)
if (NEED_RESTBED)
    target_link_libraries(${YX_LIB_NAME} restbed-shared)
endif()
if(NEED_YAML)
    target_link_libraries(${YX_LIB_NAME} yaml-cpp)
endif()
if(NEED_SOCI)
target_link_libraries(${YX_LIB_NAME} soci_core soci_postgresql)
endif()
if(NEED_JSON)
target_link_libraries(${YX_LIB_NAME} nlohmann_json::nlohmann_json)
endif()
if(NEED_CPR)
target_link_libraries(${YX_LIB_NAME} cpr)
endif()
if(NEED_ZMQ)
target_link_libraries(${YX_LIB_NAME} cppzmq)
endif()


target_include_directories(${YX_LIB_NAME}
        PUBLIC
        $<BUILD_INTERFACE:${INCLUDE_DIR}>
        $<BUILD_INTERFACE:${LIB_DEPENDECIES_DIR}/out/${YX_SOCI_NAME}/include>
        $<BUILD_INTERFACE:${LIB_DEPENDECIES_DIR}/${YX_RESTBED_NAME}/source>
        $<BUILD_INTERFACE:${LIB_DEPENDECIES_DIR}/${YX_JSON_NAME}/include>
        $<BUILD_INTERFACE:${LIB_DEPENDECIES_DIR}/${YX_CURLPP_NAME}/include>
        $<BUILD_INTERFACE:${LIB_DEPENDECIES_DIR}/${YX_YAML_NAME}/include>
        $<BUILD_INTERFACE:${LIB_DEPENDECIES_DIR}/${YX_CPPZMQ_NAME}/include>
        $<BUILD_INTERFACE:${LIB_DEPENDECIES_DIR}/${YX_CPR_NAME}/include>
        PRIVATE
        $<BUILD_INTERFACE:${SOURCE_DIR};${INCLUDE_DIR}>)
