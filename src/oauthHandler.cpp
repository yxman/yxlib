//
// Created by thephosphorus on 11/4/19.
//

#include "yxlib/oauthHandler.h"

#include <cpr/cpr.h>
#include <iostream>
#include <yxlib/ConfigHelper.h>
#include <yxlib/RestServer.h>

#include <ctime>

using namespace yx;

std::vector<std::string> OauthHandler::_clientToken;
std::string OauthHandler::_oauthHost;

std::string OauthHandler::generateToken() {
  updateClientTokenFromConf();
  srand(time(nullptr));
  return std::to_string(std::rand());
}
bool OauthHandler::isvalidToken(const std::string &userToken, size_t clientToken) {
  updateClientTokenFromConf();
  constexpr auto validationEndPoint = "/oauth/verify";
  constexpr auto userTokenField = "userToken";
  constexpr auto clientTokenField = "clientToken";

  Rest::Json body;

  body[userTokenField]=userToken;
  body[clientTokenField]=_clientToken[clientToken%_clientToken.size()];

  auto oAuthReq =
      cpr::Get(cpr::Url{OauthHandler::getOauthHost() + validationEndPoint},
                cpr::Body{body.dump()},
                cpr::Header{{"Content-Type", "application/json"}});

  return oAuthReq.status_code == restbed::OK;
}
void OauthHandler::updateClientTokenFromConf() {
  if (_clientToken.empty()) {
    try {
      const ConfigSection &section = ConfigHelper::getSection("oauth");

      _clientToken = section.getStrArr("clientToken");
      _oauthHost = section.getStr("host");
    } catch (const std::exception &exception) {
      std::cout << "Could not find Oauth Token" << std::endl;
    }
  }
}
const std::string &OauthHandler::getOauthHost() {
  updateClientTokenFromConf();
  return _oauthHost;
}
