#ifndef YXLIB_MODELS_H
#define YXLIB_MODELS_H

#include <string>
#include <vector>
#include "RestServer.h"

constexpr auto TRANSACTION_FIELD= "transaction";
constexpr auto PDF_FIELD= "pdf";
constexpr auto COURSE_ID_FIELD= "sigle";
constexpr auto NAME_FIELD= "nom";
constexpr auto TRIMESTER_FIELD= "trimestre";
constexpr auto RESULTS_FIELD= "resultats";
constexpr auto FIRST_NAME_FIELD= "prenom";
constexpr auto STUDENT_ID_FIELD= "matricule";
constexpr auto GRADE_FIELD= "note";

enum MessageType {
    STUDENT, COURSE, LOGS, EXIT, EMPTY, PING, BLOCKCHAIN
};

struct Result {
    std::string lastName;
    std::string firstName;
    std::string id;
    std::string grade;
    friend bool operator==(const Result& lhs, const Result& rhs);
};

struct Transaction {
    std::string id;
    std::string name;
    int trimester;
    std::vector<Result> results;
    friend bool operator==(const Transaction& lhs, const Transaction& rhs);
};

struct CourseRequest {
    std::string id;
    int trimester;
};

struct StudentRequest {
    std::string id;
    std::string trimester;
    std::string studentId;
};

Transaction jsonToTransaction(yx::Rest::Json jsonParams);
CourseRequest jsonToCourseRequest(yx::Rest::Json jsonParams);
StudentRequest jsonToStudentRequest(yx::Rest::Json jsonParams);

#endif //YXLIB_MODELS_H
