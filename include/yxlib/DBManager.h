//
// Created by thephosphorus on 9/30/19.
//

#ifndef USER_SERVICE_DBMANAGER_H
#define USER_SERVICE_DBMANAGER_H

#include <soci/soci.h>

#include <string>

namespace yx {

class DBManager {
public:
  DBManager(size_t type, const std::string &host, const std::string &port,
            const std::string &db, const std::string &user,
            const std::string &password);

  DBManager();

  static std::string generateConnectionString(size_t type,
                                              const std::string &host,
                                              const std::string &port,
                                              const std::string &db,
                                              const std::string &user,
                                              const std::string &password);

  soci::session &sql();

private:
  void init(size_t type, const std::string &host, const std::string &port,
            const std::string &db, const std::string &user,
            const std::string &password);

private:
  std::unique_ptr<soci::session> _session;

  static constexpr auto _dbTypes = {"postgresql", "mysql"};
};

} // namespace yx

#endif // USER_SERVICE_DBMANAGER_H
