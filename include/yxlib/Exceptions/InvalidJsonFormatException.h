#ifndef YXLIB_INVALIDJSONFORMATEXCEPTION_H
#define YXLIB_INVALIDJSONFORMATEXCEPTION_H

#include <exception>

class InvalidJsonFormatException : std::exception {};

#endif // YXLIB_INVALIDJSONFORMATEXCEPTION_H
