#include <gtest/gtest.h>
#include <yxlib.h>

std::string testStr = "test";


TEST(yxstream_test,class_creation){
    yx::ConfigHelper::addFile("testConfig.yaml");
    ASSERT_NO_THROW(yx::yxstream(testStr, &std::cout, &std::cout));
}

TEST(yxstream_test,cout){
  yx::ConfigHelper::addFile("testConfig.yaml");
  ASSERT_NO_THROW(yx::cout() << "test" << std::endl;);
}

TEST(yxstream_test,cwar){
  yx::ConfigHelper::addFile("testConfig.yaml");
  ASSERT_NO_THROW(yx::cwar() << "test" << std::endl;);
}

TEST(yxstream_test,cerr){
  yx::ConfigHelper::addFile("testConfig.yaml");
  ASSERT_NO_THROW(yx::cerr() << "test" << std::endl;);
}


TEST(yxstream_test, pipe_string){
    yx::ConfigHelper::addFile("testConfig.yaml");
    yx::yxstream yxos(testStr, &std::cout, &std::cout);
    ASSERT_NO_THROW(yxos << "test");
}


TEST(yxstream_test, pipe_int){
    yx::ConfigHelper::addFile("testConfig.yaml");
    yx::yxstream yxos(testStr, &std::cout, &std::cout);
    int a = 10;
    ASSERT_NO_THROW(yxos << a);
}

TEST(yxstream_test, pipe_endl){
    yx::ConfigHelper::addFile("testConfig.yaml");
    yx::yxstream yxos(testStr, &std::cout, &std::cout);
    ASSERT_NO_THROW(yxos << std::endl);
}
/*
TEST(yxstream_test, pipe_setw){
    yx::yxstream yxos("test", std::cout, std::cout);
    ASSERT_NO_THROW(yxos << std::setw(5));
}*/

TEST(yxstream_test, pipe_all){
    yx::ConfigHelper::addFile("testConfig.yaml");
    yx::yxstream yxos(testStr, &std::cout, &std::cout);
    int a = 10;
    ASSERT_NO_THROW(yxos << "test" << a << std::endl);
}
