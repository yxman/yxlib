//
// Created by thephosphorus on 10/2/19.
//

#include "yxlib/ConfigHelper.h"

#include <iostream>
#include <utility>

using namespace yx;

std::unique_ptr<ConfigHelper> ConfigHelper::_instance;

ConfigHelper::ConfigHelper() {}

ConfigHelper &ConfigHelper::instance() {
  if (!_instance)
    _instance = std::unique_ptr<ConfigHelper>(new ConfigHelper());

  return *_instance;
}

ConfigSection ConfigHelper::getSection(const std::string &sectionName) {
  auto &sections = ConfigHelper::instance()._sections;
  auto it = sections.find(sectionName);
  if (it == sections.end()) {
    for (const YAML::Node &config : ConfigHelper::instance()._configs) {
      try {
        const auto section = config[sectionName];
        if (!section.IsMap()) {
          std::cerr << "Section : " << sectionName << " is not a section"
                    << std::endl;
          return ConfigSection(false, sectionName, YAML::Node());
        }
        const auto newSection = ConfigSection(true, sectionName, section);
        return sections.insert(std::make_pair(sectionName, newSection))
            .first->second;
      } catch (const std::exception &exception) {
        continue;
      }
    }
    return ConfigSection(false, sectionName, YAML::Node());
  }
  return it->second;
}

bool ConfigSection::isvalid() const { return _isValid; }

const std::string &ConfigSection::name() const { return _sectionName; }

const YAML::Node &ConfigSection::raw() const { return _node; }

ConfigSection::ConfigSection(bool isValid, std::string name,
                             const YAML::Node &node)
    : _isValid(isValid), _sectionName(std::move(name)), _node(node) {}

size_t ConfigSection::getUint(const std::string &field) const {
  return get<size_t>(field);
}

std::string ConfigSection::getStr(const std::string &field) const {
  return get<std::string>(field);
}

template <typename T> T ConfigSection::get(const std::string &field) const {
  try {
    return _node[field].as<T>();
  } catch (const std::exception &exception) {
    std::cerr << "Field " << field << " doesn't exists in section "
              << _sectionName << std::endl;
    return T();
  }
}
std::vector<std::string>
ConfigSection::getStrArr(const std::string &field) const {
  std::vector<std::string> arr;

  try {
    const YAML::Node nodeArr = _node[field];

    try {
      if (nodeArr.IsSequence()) {
        for (size_t i = 0; i < nodeArr.size(); ++i) {
          arr.emplace_back(nodeArr[i].as<std::string>());
        }
      }
    } catch (const std::exception &exception) {
      std::cerr << "Element in " << field << " can't be imported "
                << _sectionName << std::endl;
      return arr;
    }
  } catch (const std::exception &exception) {
    std::cerr << "Field " << field << " doesn't exists in section "
              << _sectionName << std::endl;
    return arr;
  }

  return arr;
}
std::vector<ConfigSection>
ConfigSection::getConfigArr(const std::string &field) const {
  std::vector<ConfigSection> arr;

  try {
    const YAML::Node nodeArr = _node[field];
    try {
      if (nodeArr.IsSequence()) {
        for (size_t i = 0; i < nodeArr.size(); ++i) {
          if (nodeArr[i].IsMap())
            arr.emplace_back(true, field, nodeArr[i]);
        }
      }
    } catch (const std::exception &exception) {
      std::cerr << "Element in " << field << " can't be imported "
                << _sectionName << std::endl;
      return arr;
    }
  } catch (const std::exception &exception) {
    std::cerr << "Field " << field << " doesn't exists in section "
              << _sectionName << std::endl;
    return arr;
  }

  return arr;
}
ConfigSection ConfigSection::getConfig(const std::string &field) const {
  try {
    const YAML::Node nodeArr = _node[field];
    if( nodeArr.IsMap())
    {
      return ConfigSection(true,field,nodeArr);
    }
  } catch (const std::exception &exception) {
    std::cerr << "Field " << field << " doesn't exists in section "
              << _sectionName << std::endl;
  }
  return ConfigSection(false,field,YAML::Node());

}

bool ConfigHelper::addFile(const std::string &nameFile) {
  // Load Yaml File
  try {
    ConfigHelper::instance()._configs.emplace_back(YAML::LoadFile(nameFile));
    return true;
  } catch (const std::exception &exception) {
    return false;
  }
}
