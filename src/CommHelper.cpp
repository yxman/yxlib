#include <yxlib/RestServer.h>
#include "yxlib/CommHelper.h"
#include "yxlib/yxstream.h"
#include "yxlib/Exceptions/InvalidJsonFormatException.h"
#include "yxlib/Models.h"

using namespace yx;

std::shared_ptr<zmq::socket_t>
CommHelper::initConnection(std::shared_ptr<zmq::context_t>& context, const std::string &endpoint,
                           zmq::socket_type type, bool isBind, const std::vector<Option> &options) {

    auto socket = std::make_shared<zmq::socket_t>(*context,type);
    yx::cout() << "Liaison en mode " << int(type) <<" vers " << endpoint << " ..." << std::endl;
    if(type == zmq::socket_type::sub){
        socket->setsockopt(ZMQ_SUBSCRIBE, "", 0);
    }

    if(!options.empty()){
        for(const auto& option:options){
            socket->setsockopt(option.key, option.value);
        }
    }


    try {
        if(isBind)
            socket->bind(endpoint);
        else
            socket->connect(endpoint);

        yx::cout() << "Port lié avec succès" <<std::endl;
    }
    catch (std::exception& ex) {
        yx::cerr() << "Erreur de liaison avec " << endpoint << std::endl;
    }

    return socket;
}

std::string CommHelper::receiveString(std::shared_ptr<zmq::socket_t> &socket, zmq::recv_flags flag) {
    zmq::message_t message;
    auto res = socket->recv(message, flag);
    if (res.has_value()){
        char* msg = static_cast<char *>(message.data());
        std::string msg_str(msg, res.value());
        return msg_str;
    }
    return "";
}

bool CommHelper::sendString(std::shared_ptr<zmq::socket_t> &socket, const std::string& message, zmq::send_flags flag) {
    zmq::message_t msg(message.c_str(), message.size());
    auto ret = socket->send(msg, flag);
    return ret.has_value();
}

bool CommHelper::sendRequest(std::shared_ptr<zmq::socket_t> &socket, int type, const std::string &request, zmq::send_flags flag ) {
    yx::Rest::Json json;
    json[TYPE_FIELD] = type;
    json[REQUEST_FIELD] = request;
    std::string message = json.dump();
    zmq::message_t msg(message.c_str(), message.size());
    auto ret = socket->send(msg , flag);
    return ret.has_value();
}

CommHelper::Request CommHelper::receiveRequest(std::shared_ptr<zmq::socket_t> &socket, zmq::recv_flags flag) {
    std::string res= CommHelper::receiveString(socket, flag);
    if(!res.empty()) {
        try {
            yx::Rest::Json jsonResults = yx::Rest::Json::parse(res);
            int type = jsonResults[TYPE_FIELD];
            std::string params = jsonResults[REQUEST_FIELD];

            return {type, params};
        }catch(...){
            throw InvalidJsonFormatException();
        }
    }

    return {EMPTY ,""};
}
