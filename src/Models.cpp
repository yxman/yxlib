#include <yxlib/Exceptions/InvalidJsonFormatException.h>
#include "yxlib/Models.h"

Transaction jsonToTransaction(yx::Rest::Json jsonParams){
    yx::Rest::Json jsonResults;
    if(jsonParams.contains(RESULTS_FIELD)) {
        jsonResults = jsonParams[RESULTS_FIELD].get<std::vector<yx::Rest::Json>>();
    }
    else {
        throw InvalidJsonFormatException();
    }
    std::vector<Result> results;

    for (auto jsonResult : jsonResults) {
        if(jsonResult.contains(NAME_FIELD) &&
                jsonResult.contains(FIRST_NAME_FIELD) &&
                jsonResult.contains(STUDENT_ID_FIELD) &&
                jsonResult.contains(GRADE_FIELD)
        ) {
            Result result{
                    jsonResult[NAME_FIELD].get<std::string>(),
                    jsonResult[FIRST_NAME_FIELD].get<std::string>(),
                    jsonResult[STUDENT_ID_FIELD].get<std::string>(),
                    jsonResult[GRADE_FIELD].get<std::string>()
            };
            results.push_back(result);
        }
        else {
            throw InvalidJsonFormatException();
        }
    }

    Transaction transaction {
            jsonParams[COURSE_ID_FIELD].get<std::string>(),
            jsonParams[NAME_FIELD].get<std::string>(),
            jsonParams[TRIMESTER_FIELD].get<int>(),
            results
    };
    return transaction;
}

CourseRequest jsonToCourseRequest(yx::Rest::Json jsonParams) {
    if(jsonParams.contains(COURSE_ID_FIELD) &&
            jsonParams.contains(TRIMESTER_FIELD)) {
        CourseRequest courseRequest{
                jsonParams[COURSE_ID_FIELD].get<std::string>(),
                jsonParams[TRIMESTER_FIELD].get<int>()

        };
        return courseRequest;
    }
    else {
        throw InvalidJsonFormatException();
    }

}

StudentRequest jsonToStudentRequest(yx::Rest::Json jsonParams) {
    if(jsonParams.contains(COURSE_ID_FIELD) &&
           jsonParams.contains(TRIMESTER_FIELD) &&
           jsonParams.contains(STUDENT_ID_FIELD)) {
        StudentRequest studentRequest{
                jsonParams[COURSE_ID_FIELD].get<std::string>(),
                jsonParams[TRIMESTER_FIELD].get<std::string>(),
                jsonParams[STUDENT_ID_FIELD].get<std::string>()
        };
        return studentRequest;
    }
    else {
        throw InvalidJsonFormatException();
    }
}

bool operator==(const Transaction &lhs, const Transaction &rhs) {
  if(!(lhs.id == rhs.id && lhs.trimester == rhs.trimester && lhs.name == rhs.name
    && lhs.results.size() == rhs.results.size()))
    return false;



  for (int i = 0; i < lhs.results.size(); ++i) {
    if(!(lhs.results[i] == rhs.results[i]))
      return false;
  }
  return true;
}

bool operator==(const Result &lhs, const Result &rhs) {
  return lhs.id == rhs.id && lhs.grade == rhs.grade && lhs.lastName == rhs.lastName;
}
