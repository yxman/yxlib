#include "yxlib/yxstream.h"

#include <ctime>
#include <memory>
#include <utility>
#include "yxlib/ConfigHelper.h"

namespace yx {
unsigned yxstream::_nLog = 1;
static std::unique_ptr<yxstream> _cout;
static std::unique_ptr<yxstream> _cwar;
static std::unique_ptr<yxstream> _cerr;


yxstream& cout() {
  if(!_cout){
    _cout = std::make_unique<yxstream>(yxstream::INFO);
  }
  return *_cout;
}

yxstream& cwar() {
  if(!_cwar){
    _cwar = std::make_unique<yxstream>(yxstream::WARNING);
  }
  return *_cwar;
}


yxstream& cerr() {
  if(!_cerr){
    _cerr = std::make_unique<yxstream>(yxstream::ERROR);
  }
  return *_cerr;
}


yxstream::yxstream(TYPE type) {
  loadConfig();
  std::time_t currTime = std::time(nullptr);
  std::tm *localTime = std::localtime(&currTime);

  std::string dateString = std::to_string(localTime->tm_year + 1900);
  dateString += "-" + std::to_string(localTime->tm_mon + 1);
  dateString += "-" + std::to_string(localTime->tm_mday);

  constexpr auto infoFileName = "access.log";
  _openedFiles.emplace_back(std::make_unique<std::ofstream>(_logPath + infoFileName, std::ios::out | std::ios::app));
  _osStreams.emplace_back(_openedFiles.back().get());

  if (type == ERROR) {
    constexpr auto errorFileName = "error.log";
    _openedFiles.emplace_back(std::make_unique<std::ofstream>(_logPath + errorFileName, std::ios::out | std::ios::app));
    _osStreams.emplace_back(_openedFiles.back().get());
    _osStreams.emplace_back(&std::cerr);
  } else {
    _osStreams.emplace_back(&std::cout);
  }
  std::string logTypes[3] = {"INFO", "ATTENTION", "ERREUR"};
  _type = logTypes[type];
}

yxstream::yxstream(std::string &type, std::ostream *os1, std::ostream *os2)
    : _type(type) {
  loadConfig();
  _osStreams.push_back(os1);
  _osStreams.push_back(os2);
}

yxstream::yxstream(std::string &type, std::ostream *os1, std::ostream *os2,
                   std::ostream *os3)
    : _type(type) {
  loadConfig();
  _osStreams.push_back(os1);
  _osStreams.push_back(os2);
  _osStreams.push_back(os3);
}

yxstream::yxstream(std::string &type, std::vector<std::ostream *> os)
    : _type(type), _osStreams(std::move(os)) {
  loadConfig();
}

void yxstream::loadConfig() {
  auto section = ConfigHelper::getSection("yxstream");
  _location = section.getStr("app");
  _logPath = section.getStr("logPath");
  if(!_logPath.empty() and _logPath.back() != '/')
    _logPath+='/';
}


// Redirect manipulator element
yxstream &operator<<(yxstream &yxos,
                     std::ostream &(*manipElement)(std::ostream &)) {

  if (yxos._isNewLog) { // if it is a new log, add the header
    yxos._isNewLog =
        false; // after adding header, we are in the same log until an endl
    yxos.updateHeader();
  }
  for (std::ostream *stream : yxos._osStreams) {
    *stream << manipElement;
  }
  yxos._isNewLog = true; // after an endl or flush, the next input is a new log

  return yxos;
}

void yxstream::updateHeader() {
  LogHeader logHeader(_nLog++, _location, _type);
  *this << logHeader;
}


} // namespace yx
