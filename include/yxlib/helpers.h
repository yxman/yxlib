#ifndef YXLIB_HELPERS_H
#define YXLIB_HELPERS_H

#include <ctime>
#include <ostream>

// Functor to pipe log info
class LogHeader {
private:
  unsigned _nLog;
  std::string _logSource;
  std::string _type;
  tm *_time;

public:
  LogHeader(unsigned logID, std::string &logSource, std::string &type);
  friend std::ostream &operator<<(std::ostream &os, const LogHeader &lh);
};

#endif // YXLIB_HELPERS_H