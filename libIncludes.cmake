## Add GIT
find_package(Git QUIET)
## Add boost
find_package(Boost REQUIRED COMPONENTS filesystem)

## Add RestBed
if (NEED_RESTBED)
    set(YX_RESTBED_NAME restbed)
    if (NOT DEFINED YX_IMPORTED_RESTBED)
        set(YX_IMPORTED_RESTBED ON)
        if (NOT EXISTS ${LIB_DEPENDECIES_DIR}/${YX_RESTBED_NAME})
            message(STATUS "Cloning ${YX_RESTBED_NAME}")
            execute_process(COMMAND ${GIT_EXECUTABLE} clone --recurse-submodules
                    https://github.com/Corvusoft/restbed.git ${LIB_DEPENDECIES_DIR}/${YX_RESTBED_NAME})
        else ()
            message(STATUS "${YX_RESTBED_NAME} Already found")
        endif ()
    endif ()

    set(BUILD_STATIC OFF CACHE INTERNAL "")
    set(BUILD_SHARED ON CACHE INTERNAL "")
    set(BUILD_SSL OFF CACHE INTERNAL "")
    set(BUILD_TESTS OFF CACHE INTERNAL "")

    add_subdirectory("${LIB_DEPENDECIES_DIR}/${YX_RESTBED_NAME}" "${LIB_DEPENDECIES_DIR}/out/${YX_RESTBED_NAME}")
    include_directories("${LIB_DEPENDECIES_DIR}/${YX_RESTBED_NAME}/source")
endif ()


## Add YAML-CPP
if (NEED_YAML)
    set(YX_YAML_NAME yaml)
    if (NOT DEFINED YX_IMPORTED_YAML)
        set(YX_IMPORTED_YAML ON)
        if (NOT EXISTS ${LIB_DEPENDECIES_DIR}/${YX_YAML_NAME})
            message(STATUS "Cloning ${YX_YAML_NAME}")
            execute_process(COMMAND ${GIT_EXECUTABLE} clone --recurse-submodules
                    https://github.com/jbeder/yaml-cpp.git ${LIB_DEPENDECIES_DIR}/${YX_YAML_NAME})
        else ()
            message(STATUS "${YX_YAML_NAME} Already found")
        endif ()
    endif ()

    set(YAML_BUILD_SHARED_LIBS ON CACHE INTERNAL "")
    add_subdirectory("${LIB_DEPENDECIES_DIR}/${YX_YAML_NAME}" "${LIB_DEPENDECIES_DIR}/out/${YX_YAML_NAME}")
endif ()

## Add SOCI
if (NEED_SOCI)
    set(YX_SOCI_NAME soci)
    if (NOT DEFINED YX_IMPORTED_SOCI)
        set(YX_IMPORTED_SOCI ON)
        if (NOT EXISTS ${LIB_DEPENDECIES_DIR}/${YX_SOCI_NAME})
            message(STATUS "Cloning ${YX_SOCI_NAME}")
            execute_process(COMMAND ${GIT_EXECUTABLE} clone --recurse-submodules
                    https://github.com/SOCI/soci.git ${LIB_DEPENDECIES_DIR}/${YX_SOCI_NAME})
        else ()
            message(STATUS "${YX_SOCI_NAME} Already found")
        endif ()
    endif ()

    set(SOCI_SHARED ON CACHE INTERNAL "")
    set(SOCI_STATIC OFF CACHE INTERNAL "")
    set(WITH_BOOST ON CACHE INTERNAL "")
    set(SOCI_TESTS OFF CACHE INTERNAL "")
    add_subdirectory("${LIB_DEPENDECIES_DIR}/${YX_SOCI_NAME}" "${LIB_DEPENDECIES_DIR}/out/${YX_SOCI_NAME}")
    include_directories("${LIB_DEPENDECIES_DIR}/out/${YX_SOCI_NAME}/include")
    message("${LIB_DEPENDECIES_DIR}/out/${YX_SOCI_NAME}/include")
endif ()


## Add JSON
if (NEED_JSON)
    set(YX_JSON_NAME json)
    if (NOT DEFINED YX_IMPORTED_JSON)
        set(YX_IMPORTED_JSON ON)
        if (NOT EXISTS ${LIB_DEPENDECIES_DIR}/${YX_JSON_NAME})
            message(STATUS "Cloning ${YX_JSON_NAME}")
            execute_process(COMMAND ${GIT_EXECUTABLE} clone --recurse-submodules
                    https://github.com/nlohmann/json.git ${LIB_DEPENDECIES_DIR}/${YX_JSON_NAME})
        else ()
            message(STATUS "${YX_JSON_NAME} Already found")
        endif ()
    endif ()

    set(JSON_BuildTests OFF CACHE INTERNAL "")
    add_subdirectory("${LIB_DEPENDECIES_DIR}/${YX_JSON_NAME}" "${LIB_DEPENDECIES_DIR}/out/${YX_JSON_NAME}")
endif ()


## Add curlpp
if (NEED_CPR)
    set(YX_CPR_NAME cpr)
    if (NOT DEFINED YX_IMPORTED_CPR)
        set(YX_IMPORTED_CPR ON)
        if (NOT EXISTS ${LIB_DEPENDECIES_DIR}/${YX_CPR_NAME})
            message(STATUS "Cloning ${YX_CPR_NAME}")
            execute_process(COMMAND ${GIT_EXECUTABLE} clone --recurse-submodules
                    https://github.com/whoshuu/cpr.git ${LIB_DEPENDECIES_DIR}/${YX_CPR_NAME})
            message(STATUS "Executing : sed -i '4i SHARED' ${LIB_DEPENDECIES_DIR}/${YX_CPR_NAME}/cpr/CMakeLists.txt")
            execute_process(COMMAND "sed -i '4i SHARED' ${LIB_DEPENDECIES_DIR}/${YX_CPR_NAME}/cpr/CMakeLists.txt")
        else ()
            message(STATUS "${YX_CPR_NAME} Already found")
        endif ()
    endif ()

    set(CMAKE_USE_OPENSSL OFF CACHE INTERNAL "")
    set(BUILD_CPR_TESTS OFF CACHE INTERNAL "")
    set(USE_SYSTEM_CURL ON CACHE INTERNAL "")
    add_subdirectory("${LIB_DEPENDECIES_DIR}/${YX_CPR_NAME}" "${LIB_DEPENDECIES_DIR}/out/${YX_CPR_NAME}")
endif ()

## Add ZMQ
if (NEED_ZMQ)
    set(YX_CPPZMQ_NAME cppzmq)
    if (NOT DEFINED YX_IMPORTED_CPPZMQ)
        set(YX_IMPORTED_CPPZMQ ON)
        if (NOT EXISTS ${LIB_DEPENDECIES_DIR}/${YX_CPPZMQ_NAME})
            message(STATUS "Cloning ${YX_CPPZMQ_NAME}")
            execute_process(COMMAND ${GIT_EXECUTABLE} clone --recurse-submodules
                    https://github.com/zeromq/cppzmq.git ${LIB_DEPENDECIES_DIR}/${YX_CPPZMQ_NAME})
        else ()
            message(STATUS "${YX_CPPZMQ_NAME} Already found")
        endif ()
    endif ()
    set(CPPZMQ_BUILD_TESTS FALSE)
    add_subdirectory("${LIB_DEPENDECIES_DIR}/${YX_CPPZMQ_NAME}" "${LIB_DEPENDECIES_DIR}/out/${YX_CPPZMQ_NAME}")
endif ()
