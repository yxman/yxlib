//
// Created by thephosphorus on 11/4/19.
//

#ifndef USER_SERVICE_OAUTHHANDLER_H
#define USER_SERVICE_OAUTHHANDLER_H

#include <string>
#include <vector>

class OauthHandler {
public:
  static std::string generateToken();

  static bool isvalidToken(const std::string& userToken, size_t clientToken = 0);

  static const std::string& getOauthHost();

private:
  static void updateClientTokenFromConf();

private:
  static std::vector<std::string> _clientToken;
  static std::string _oauthHost;

};

#endif // USER_SERVICE_OAUTHHANDLER_H
