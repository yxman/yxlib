//
// Created by thephosphorus on 10/2/19.
//

#ifndef USER_SERVICE_CONFIGHELPER_H
#define USER_SERVICE_CONFIGHELPER_H

#include <yaml-cpp/yaml.h>

#include <memory>
#include <string>
#include <unordered_map>

namespace yx {

class ConfigSection {
public:
  ConfigSection(bool isValid, std::string name, const YAML::Node &node);

  bool isvalid() const;

  const std::string &name() const;

  const YAML::Node &raw() const;

  size_t getUint(const std::string &field) const;

  std::string getStr(const std::string &field) const;

  std::vector<std::string> getStrArr(const std::string & field) const;

  std::vector<ConfigSection> getConfigArr(const std::string& field) const;

  ConfigSection getConfig(const std::string& field) const;


  template <typename T> T get(const std::string &field) const;

private:
  const bool _isValid;
  const std::string _sectionName;
  const YAML::Node _node;
};

class ConfigHelper {
public:
  static ConfigHelper &instance();

  static bool addFile(const std::string &nameFile);

  static ConfigSection getSection(const std::string &sectionName);

private:
  ConfigHelper();

private:
  static std::unique_ptr<ConfigHelper> _instance;
  std::vector<YAML::Node> _configs;
  std::unordered_map<std::string, ConfigSection> _sections;
};

} // namespace yx

#endif // USER_SERVICE_CONFIGHELPER_H
